<?php

namespace makeitbetter\thumbnail;

/**
 * Class for creating thumbnails.
 */
class Thumbnail {

    /**
     * Path to folder with images source.
     * @var string
     */
    public static $sourceFolder = '/path/to/source';

    /**
     * Path to folder for thumbnails relative to the webroot.
     * @var string
     */
    public static $targetFolder = '/path/to/target';

    /**
     * Path to web root
     * @var string
     */
    public static $webRoot;

    /**
     * Hexadecimal RGB color to fill thumbnails padding.
     * @var string
     */
    public static $paddingColor = 'ffffff';

    /**
     *
     * @var callable
     */
    public static $getThumbnailPathMethod;

    /**
     * Path to the image relative to the source folder.
     * @var string
     */
    public $path = '';

    /**
     * Whether to stretch the image whose size is less than the size of the thumbnail.
     * @var boolean
     */
    public $stretch = false;

    /**
     * Whether to crop the image whose proportions do not match the proportions of the thumbnail.
     * @var boolean
     */
    public $crop = false;

    /**
     * Thumbnails width.
     * @var int
     */
    public $width = 0;

    /**
     * Thumbnails height.
     * @var int
     */
    public $height = 0;

    /**
     * Returns thumbnails object.
     * @param tring $path
     * @param int $width
     * @param int $height
     * @param boolean $crop
     * @param boolean $stretch
     * @return Thumbnail
     */
    public static function of($path, $width = 0, $height = 0, $crop = false, $stretch = false)
    {
        return new static($path, $width, $height, $crop, $stretch);
    }

    protected function __construct($path, $width, $height, $crop, $stretch)
    {
        $this->path = trim($path, " \n\r\t\\\/");
        $this->width = intval($width);
        $this->height = intval($height);
        $this->crop = boolval($crop);
        $this->stretch = boolval($stretch);
    }

    /**
     * Sets the thumbnails width.
     * @param int $width
     * @return $this
     */
    public function width($width)
    {
        $this->width = intval($width);
        $this->height = 0;
        return $this;
    }

    /**
     * Sets the thumbnails height.
     * @param int $height
     * @return $this
     */
    public function height($height)
    {
        $this->height = intval($height);
        $this->width = 0;
        return $this;
    }

    /**
     * Sets the exact size of thumbnails.
     * @param int $width
     * @param int $height
     * @return $this
     */
    public function size($width, $height)
    {
        $this->width = intval($width);
        $this->height = intval($height);
        return $this;
    }

    /**
     * Crops the image if its proportions do not match the proportions of the thumbnail.
     * @return $this
     */
    public function crop()
    {
        $this->crop = true;
        return $this;
    }

    /**
     * Scales the image such it can fit inside thumbnails frame (with adding padding).
     * @return $this
     */
    public function contain()
    {
        $this->crop = false;
        return $this;
    }

    /**
     * Stretches small image to thumbnails size.
     * @return $this
     */
    public function stretch()
    {
        $this->stretch = true;
        return $this;
    }

    /**
     * Leaves the size of small images as is (with adding padding).
     * @return $this
     */
    public function nostretch()
    {
        $this->stretch = false;
        return $this;
    }

    /**
     * Creates a thumbnail. Returns a path to it.
     * @return  string
     */
    public function path()
    {
        $srcPath = static::$sourceFolder . '/' . $this->path;

        if (static::$getThumbnailPathMethod) {
            $thumbnailPath = call_user_func(static::$getThumbnailPathMethod, $this);
        }
        else {
            $thumbnailPath = $this->_getThumbnailPath();
        }
        $webroot = self::$webRoot ? : $_SERVER['DOCUMENT_ROOT'];
        $fullPath = $webroot . '/' . static::$targetFolder . '/' . $thumbnailPath;
        $dirPath = pathinfo($fullPath,  PATHINFO_DIRNAME);

        if (!is_dir($dirPath)) {
            mkdir($dirPath, 0777, true);
        }
        if (!is_file($fullPath) && is_file($srcPath)) {
            if (!$this->width && !$this->height) {
                copy($srcPath, $fullPath);
            }
            else {
                $this->_createThumbnail($srcPath, $fullPath);
            }
        }
        return '/' . static::$targetFolder . '/' . $thumbnailPath;
    }

    /**
     * Creates a thumbnail. Returns HTML-teg <img/>.
     * @param array	$attributes
     * @return  string
     */
    public function img($attributes = [])
    {
        $thumbnailPath = $this->path();
        $attrString = '';
        foreach ($attributes as $key => $value) {
            $attrString .= ' ' . $key . '="' . $value . '"';
        }
        return '<img src="' . $thumbnailPath . '"' . $attrString . '/>';
    }

    public function __toString()
    {
        return $this->path();
    }

    /**
     * Creates a thumbnail.
     * @param 	string	$sourcePath
     * @param	string	$targetPath
     */
    protected function _createThumbnail($sourcePath, $targetPath)
    {
        if (!is_file($sourcePath)) {
            return;
        }
        if (!$this->width && !$this->height) {
            return;
        }
        $imageSize = getImageSize($sourcePath);
        $srcW = $imageSize[0];
        $srcH = $imageSize[1];
        $srcRatio = $srcW / $srcH;
        switch ($imageSize[2]) {
            case 1 : $source = imageCreateFromGif($sourcePath);
                break;
            case 2 : $source = imageCreateFromJpeg($sourcePath);
                break;
            case 3 : $source = imageCreateFromPng($sourcePath);
                break;
            default: return;
        }
        if (!$this->width) { // Only height is setting.
            list($frameW, $frameH, $dstW, $dstH, $dstX, $dstY, $srcW, $srcH, $srcX, $srcY) = $this->_calculatePointsByOneSide($this->height, $srcW, $srcH);
        }
        elseif (!$this->height) { // Only width is setting.
            list($frameH, $frameW, $dstH, $dstW, $dstY, $dstX, $srcH, $srcW, $srcY, $srcX) = $this->_calculatePointsByOneSide($this->width, $srcH, $srcW);
        }
        else { // Both height and width are setting.
            $frameW = $this->width;
            $frameH = $this->height;
            $ratio = $this->width / $this->height;
            if ($ratio < $srcRatio) { // target ▋VS. ▄ source
                list($srcX, $srcY, $srcW, $srcH, $dstX, $dstY, $dstW, $dstH) = $this->_calculatePointsForFrame($frameW, $frameH, $srcW, $srcH);
            }
            else { // target ▄ VS. ▋source
                list($srcY, $srcX, $srcH, $srcW, $dstY, $dstX, $dstH, $dstW) = $this->_calculatePointsForFrame($frameH, $frameW, $srcH, $srcW);
            }
        }
        $canvas = imageCreateTrueColor($frameW, $frameH);

        $color = imageColorAllocate($canvas, hexdec(static::$paddingColor[0] . static::$paddingColor[1]), hexdec(static::$paddingColor[2] . static::$paddingColor[3]), hexdec(static::$paddingColor[4] . static::$paddingColor[5]));
        imageFill($canvas, 0, 0, $color);

        if ($imageSize[2] != 2) {
            imageColorTransparent($canvas, $color);
        }

        imageCopyResampled($canvas, $source, $dstX, $dstY, $srcX, $srcY, $dstW, $dstH, $srcW, $srcH);
        switch ($imageSize[2]) {
            case 1 : imageGif($canvas, $targetPath);
                break;
            case 2 : imageJpeg($canvas, $targetPath);
                break;
            case 3 : imagePng($canvas, $targetPath);
                break;
        }
        return;
    }

    /**
     *
     * @param int $height
     * @param int $srcW
     * @param int $srcH
     * @return array [frameW, frameH, $dstW, $dstH, $dstX, $dstY, $srcW, $srcH, $srcX, $srcY]
     */
    protected function _calculatePointsByOneSide($height, $srcW, $srcH)
    {
        if (!$this->stretch && $height > $srcH) {
            $dstW = $srcW;
            $dstH = $srcH;
            $dstY = floor(($height - $srcH) / 2);
        }
        else {
            $dstH = $height;
            $dstW = ceil($dstH * $srcW / $srcH);
        }
        return [$dstW, $height, $dstW, $dstH, 0, $dstY, $srcW, $srcH, 0, 0];
    }

    /**
     *
     * @param type $frameW
     * @param type $frameH
     * @param type $srcW
     * @param type $srcH
     * @return array [$srcX, $srcY, $srcW, $srcH, $dstX, $dstY, $dstW, $dstH]
     */
    protected function _calculatePointsForFrame($frameW, $frameH, $srcW, $srcH)
    {
        if ($this->crop) {
            if ($this->stretch || $srcH >= $frameH) {
                $scale = $srcH / $frameH;
                $dstH = $frameH;
                $dstW = $frameW;
                $dstX = 0;
                $dstY = 0;
                $srcX = ($srcW - $frameW * $scale) / 2;
                $srcY = 0;
                $srcW = $srcW - 2 * $srcX;
            }
            else {
                $dstH = $srcH;
                $dstW = $srcW > $frameW ? $frameW : $srcW;
                $dstX = $srcW > $frameW ? 0 : ($frameW - $srcW) / 2;
                $dstY = ($frameH - $srcH) / 2;
                $srcX = $srcW > $frameW ? ($srcW - $frameW) / 2 : 0;
                $srcY = 0;
                $srcW = $srcW > $frameW ? $frameW : $srcW;
            }
        }
        else {
            $srcX = 0;
            $srcY = 0;
            if ($this->stretch || $srcW > $frameW) {
                $scale = $srcW / $frameW;
                $dstH = $srcH / $scale;
                $dstW = $frameW;
                $dstX = 0;
                $dstY = ($frameH - $srcH / $scale) / 2;
            }
            else {
                $dstH = $srcH;
                $dstW = $srcW;
                $dstX = ($frameW - $srcW) / 2;
                $dstY = ($frameH - $srcH) / 2;
            }
        }
        return [$srcX, $srcY, $srcW, $srcH, $dstX, $dstY, $dstW, $dstH];
    }

    /**
     *
     * @return string
     */
    protected function _getThumbnailPath()
    {
        $ext = pathinfo($this->path, PATHINFO_EXTENSION);
        $hash = md5("$this->path|$this->width|$this->height|$this->crop|$this->stretch");
        return substr($hash, 0, 2) . '/' . substr($hash, 2) . '.' . $ext;
    }

}
**Thumbnail** - PHP class for thumbnails creation.

Demo: [makeitbetter.su/projects/thumbnail](http://makeitbetter.su/projects/thumbnail#demo)

## Public Variables##

*public static* **$sourceFolder**
Path to folder with images source.

*public static* **$targetFolder**
Path to folder for thumbnails relative to the webroot.

*public static* **$webRoot**
Path to web root. Default - $_SERVER['DOCUMENT_ROOT']

*public static* **$paddingColor**
Hexadecimal RGB color to fill thumbnails padding. Default - ffffff.

*public static* **$getThumbnailPathMethod**
Function that will create thumbnails path. Default - $this->_getThumbnailPath()

*public* **$path**
Path to the image relative to the source folder.

*public* **$stretch**
Whether to stretch the image whose size is less than the size of the thumbnail. Default - false

*public* **$crop**
Whether to crop the image whose proportions do not match the proportions of the thumbnail. Default - false

*public* **$width**
Thumbnails width. Default - 0 (Calculate using a source proportions)

*public* **$height**
Thumbnails height. Default - 0 (Calculate using a source proportions)

## Public methods ##

*public static* **of($path, $width = 0, $height = 0, $crop = false, $stretch = false)**
Returns new object.

*public* **width($width)**
Sets the thumbnails width. Height will be calculated using a source proportions. Returns its self.

*public* **height($height)**
Sets the thumbnails height. Width will be calculated using a source proportions. Returns its self.

*public* **size($width, $height)**
Sets the exact size of thumbnails. Returns its self.

*public* **crop()**
Crops the image if its proportions do not match the proportions of the thumbnail. Returns its self.

*public* **contain()**
Scales the image such it can fit inside thumbnails frame (with adding padding). Returns its self.

*public* **stretch()**
Stretches small image to thumbnails size. Returns its self.

*public* **nostretch()**
Leaves the size of small images as is (with adding padding). Returns its self.

*public* **path()**
Creates a thumbnail if it absent. Returns a path to it. This method will be called when trying to convert an object to a string:


```
#!php
<img src="<?php echo Thumbnail::of('images/example.jpg', 100)->path();?>" />
<!-- equals : -->
<img src="<?php echo Thumbnail::of('images/example.jpg', 100);?>" />
```

*public* **img($attributes = [])**
Wraps a path into a HTML-tag <img/>:

```
#!php
<img src="<?php echo Thumbnail::of('images/example.jpg', 100);?>" class="thumbnail" />
<!-- equals : -->
<?php echo Thumbnail::of('images/example.jpg', 100)->img(['class' => 'thumbnail']);?>
```